/*
 * File:   device.h
 * Author: Plentify Hardware Team
 */

#ifndef DEVICE_H
#define	DEVICE_H

#include "base.h"

#define FULL_REGISTER_MASK              0xFFFFFFFF
#define H_HALFWORD_MASK                 0xFFFF0000
#define L_HALFWORD_MASK                 0x0000FFFF
#define BYTE_MASK                       0xFF
#define BITS_IN_WORD_SHIFT              5
#define BITS_IN_REGISTER                32
#define PBCLK_FREQ                      8000000L

typedef const struct
{
    void (*lock) (void);
    void (*unlock) (void);
    boolean_state_t (*is_locked) (void);
}device_interface_t;
extern device_interface_t device;

#endif	/* DEVICE_H */
