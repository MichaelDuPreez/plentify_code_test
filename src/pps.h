/*
 * File:   pps.h
 * Author: Plentify Hardware Team
 */

#ifndef PPS_H
#define PPS_H

#include "base.h"
#include "pinmap.h"
#include "device.h"

// Offsets to access the registers, divide by 4 used as array indexing indexes by word not byte
#define RPCON_OFFSET           0x00 >> 2
#define RPINR1_OFFSET          0x20 >> 2
#define RPINR2_OFFSET          0x30 >> 2
#define RPINR3_OFFSET          0x40 >> 2
#define RPINR5_OFFSET          0x60 >> 2
#define RPINR6_OFFSET          0x70 >> 2
#define RPINR7_OFFSET          0x80 >> 2
#define RPINR8_OFFSET          0x90 >> 2
#define RPINR9_OFFSET          0xa0 >> 2
#define RPINR10_OFFSET         0xb0 >> 2
#define RPINR11_OFFSET         0xc0 >> 2
#define RPINR12_OFFSET         0xd0 >> 2

#define mPPS_INPUT_FUNCTION_POSITION(register, function) ((RPINR##register##_OFFSET) << BITS_IN_WORD_SHIFT) | (_RPINR##register##_##function##_POSITION)

typedef enum
{
    pps_input_function_select_external_4                    = mPPS_INPUT_FUNCTION_POSITION(1, INT4R),  
    pps_input_function_select_ccp1_input_capture            = mPPS_INPUT_FUNCTION_POSITION(2, ICM1R),  
    pps_input_function_select_ccp2_input_capture            = mPPS_INPUT_FUNCTION_POSITION(2, ICM2R),  
    pps_input_function_select_ccp3_input_capture            = mPPS_INPUT_FUNCTION_POSITION(3, ICM3R),  
    pps_input_function_select_ccp4_input_capture            = mPPS_INPUT_FUNCTION_POSITION(3, ICM4R),  
    pps_input_function_select_output_compare_fault_a        = mPPS_INPUT_FUNCTION_POSITION(5, OCFAR),  
    pps_input_function_select_output_compare_fault_b        = mPPS_INPUT_FUNCTION_POSITION(5, OCFBR),  
    pps_input_function_select_ccp_clock_input_a             = mPPS_INPUT_FUNCTION_POSITION(6, TCKIAR),  
    pps_input_function_select_ccp_clock_input_b             = mPPS_INPUT_FUNCTION_POSITION(6, TCKIBR),
    pps_input_function_select_ccp5_input_capture            = mPPS_INPUT_FUNCTION_POSITION(7, ICM5R),  
    pps_input_function_select_ccp6_input_capture            = mPPS_INPUT_FUNCTION_POSITION(7, ICM6R),  
    pps_input_function_select_ccp7_input_capture            = mPPS_INPUT_FUNCTION_POSITION(7, ICM7R),  
    pps_input_function_select_ccp8_input_capture            = mPPS_INPUT_FUNCTION_POSITION(7, ICM8R),  
    pps_input_function_select_ccp9_input_capture            = mPPS_INPUT_FUNCTION_POSITION(8, ICM9R),  
    pps_input_function_select_uart3_rx                      = mPPS_INPUT_FUNCTION_POSITION(8, U3RXR),  
    pps_input_function_select_uart2_rx                      = mPPS_INPUT_FUNCTION_POSITION(9, U2RXR),  
    pps_input_function_select_uart2_cts                     = mPPS_INPUT_FUNCTION_POSITION(9, U2CTSR),  
    pps_input_function_select_uart3_cts                     = mPPS_INPUT_FUNCTION_POSITION(10, U3CTSR),  
    pps_input_function_select_spi2_data                     = mPPS_INPUT_FUNCTION_POSITION(11, SDI2R),  
    pps_input_function_select_spi2_clock                    = mPPS_INPUT_FUNCTION_POSITION(11, SCK2INR),  
    pps_input_function_select_spi2_slave_select             = mPPS_INPUT_FUNCTION_POSITION(11, SS2INR),  
    pps_input_function_select_clc_a                         = mPPS_INPUT_FUNCTION_POSITION(12, CLCINAR),  
pps_input_function_select_clc_b                             = mPPS_INPUT_FUNCTION_POSITION(12, CLCINBR),
} pps_input_function_select_t;

typedef enum
{
    pps_output_function_select_reset,   
    pps_output_function_select_comparator_1,   
    pps_output_function_select_comparator_2,   
    pps_output_function_select_comparator_3,   
    pps_output_function_select_uart2_tx,   
    pps_output_function_select_uart2_rts,   
    pps_output_function_select_uart3_tx,   
    pps_output_function_select_uart3_rts,   
    pps_output_function_select_spi2_data,   
    pps_output_function_select_spi2_clock,   
    pps_output_function_select_spi2_slave_select,   
    pps_output_function_select_ccp4_output_compare,   
    pps_output_function_select_ccp5_output_compare,   
    pps_output_function_select_ccp6_output_compare,   
    pps_output_function_select_ccp7_output_compare,   
    pps_output_function_select_ccp8_output_compare,   
    pps_output_function_select_ccp9_output_compare,   
    pps_output_function_select_clc1,   
    pps_output_function_select_clc2,   
    pps_output_function_select_clc3,   
    pps_output_function_select_clc4,   
} pps_output_function_select_t;

// Interface
typedef const struct
{
    void (*lock) (void);                                                        // Must be system unlocked
    void (*unlock) (void);                                                      // Must be system unlocked
    boolean_state_t (*is_locked) (void);
    void(*set_output_function)  (pinmap_pin_t* pin, pps_output_function_select_t function);
    void(*set_input_function)  (pinmap_pin_t* pin, pps_input_function_select_t function);
    
} pps_interface_t;
extern pps_interface_t pps;

#endif /* PPS_H */
