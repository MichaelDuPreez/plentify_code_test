/* 
 * File:   pwr.h
 * Author: Plentify Hardware Team
 */

#ifndef PWR_H
#define	PWR_H

#include <xc.h>
#include "base.h"
#include "device.h"

// Offsets to access the registers, divide by 4 used as array indexing indexes by word not byte
#define PMDCON_OFFSET                   (0x00 >> 2)
#define PMD1_OFFSET                     (0x10 >> 2)
#define PMD2_OFFSET                     (0x20 >> 2)
#define PMD3_OFFSET                     (0x30 >> 2)
#define PMD4_OFFSET                     (0x40 >> 2)
#define PMD5_OFFSET                     (0x50 >> 2)
#define PMD6_OFFSET                     (0x60 >> 2)
#define PMD7_OFFSET                     (0x70 >> 2)

#define mPWR_PERIPHERAL_POSITION(register, function) ((PMD##register##_OFFSET) << BITS_IN_WORD_SHIFT) | (_PMD##register##_##function##_POSITION)

// creating a bit position for each peripheral offset from the base address.
typedef enum
{
    pwr_peripheral_select_adc                       = mPWR_PERIPHERAL_POSITION(1, ADCMD),
    pwr_peripheral_select_voltage_reference         = mPWR_PERIPHERAL_POSITION(1, VREFMD),
    pwr_peripheral_select_high_low_voltage_detect   = mPWR_PERIPHERAL_POSITION(1, HLVDMD),
    pwr_peripheral_select_comparator_1              = mPWR_PERIPHERAL_POSITION(2, CMP1MD),
    pwr_peripheral_select_comparator_2              = mPWR_PERIPHERAL_POSITION(2, CMP2MD),
    pwr_peripheral_select_comparator_3              = mPWR_PERIPHERAL_POSITION(2, CMP3MD),
    pwr_peripheral_select_clc1                      = mPWR_PERIPHERAL_POSITION(2, CLC1MD),
    pwr_peripheral_select_clc2                      = mPWR_PERIPHERAL_POSITION(2, CLC2MD),
    pwr_peripheral_select_clc3                      = mPWR_PERIPHERAL_POSITION(2, CLC3MD),
    pwr_peripheral_select_clc4                      = mPWR_PERIPHERAL_POSITION(2, CLC4MD),
    pwr_peripheral_select_ccp1                      = mPWR_PERIPHERAL_POSITION(3, CCP1MD),
    pwr_peripheral_select_ccp2                      = mPWR_PERIPHERAL_POSITION(3, CCP2MD),
    pwr_peripheral_select_ccp3                      = mPWR_PERIPHERAL_POSITION(3, CCP3MD),
    pwr_peripheral_select_ccp4                      = mPWR_PERIPHERAL_POSITION(3, CCP4MD),
    pwr_peripheral_select_ccp5                      = mPWR_PERIPHERAL_POSITION(3, CC5MD),
    pwr_peripheral_select_ccp6                      = mPWR_PERIPHERAL_POSITION(3, CC6MD),
    pwr_peripheral_select_ccp7                      = mPWR_PERIPHERAL_POSITION(3, CC7MD),
    pwr_peripheral_select_ccp8                      = mPWR_PERIPHERAL_POSITION(3, CC8MD),
    pwr_peripheral_select_ccp9                      = mPWR_PERIPHERAL_POSITION(3, CC9MD),
    pwr_peripheral_select_timer_1                   = mPWR_PERIPHERAL_POSITION(4, T1MD),
    pwr_peripheral_select_timer_2                   = mPWR_PERIPHERAL_POSITION(4, T2MD),
    pwr_peripheral_select_timer_3                   = mPWR_PERIPHERAL_POSITION(4, T3MD),
    pwr_peripheral_select_uart1                     = mPWR_PERIPHERAL_POSITION(5, U1MD),
    pwr_peripheral_select_uart2                     = mPWR_PERIPHERAL_POSITION(5, U2MD),
    pwr_peripheral_select_uart3                     = mPWR_PERIPHERAL_POSITION(5, U3MD),
    pwr_peripheral_select_spi1                      = mPWR_PERIPHERAL_POSITION(5, SPI1MD),
    pwr_peripheral_select_spi2                      = mPWR_PERIPHERAL_POSITION(5, SPI2MD),
    pwr_peripheral_select_spi3                      = mPWR_PERIPHERAL_POSITION(5, SPI3MD),
    pwr_peripheral_select_i2c1                      = mPWR_PERIPHERAL_POSITION(5, I2C1MD),
    pwr_peripheral_select_i2c2                      = mPWR_PERIPHERAL_POSITION(5, I2C2MD),
    pwr_peripheral_select_i2c3                      = mPWR_PERIPHERAL_POSITION(5, I2C3MD),
    pwr_peripheral_select_usb                       = mPWR_PERIPHERAL_POSITION(5, USBMD),
    pwr_peripheral_select_rtcc                      = mPWR_PERIPHERAL_POSITION(6, RTCCMD),
    pwr_peripheral_select_refo1                     = mPWR_PERIPHERAL_POSITION(6, REFOMD),
    pwr_peripheral_select_dma                       = mPWR_PERIPHERAL_POSITION(7, DMAMD),     
}pwr_peripheral_select_t;

typedef enum
{
    pwr_power_saving_mode_idle,
    pwr_power_saving_mode_sleep,
    pwr_power_saving_mode_standby_sleep,
    pwr_power_saving_mode_retention_sleep,
}pwr_power_saving_mode_t;

typedef const struct
{
    void (*enter_power_saving_mode) (void);
    void (*unlock_PMD) (void);                                                  // Must unlock before using this function
    void (*lock_PMD) (void);                                                    // Must unlock before using this function
    boolean_state_t (*is_PMD_locked) (void); 
    void (*enable_peripheral) (pwr_peripheral_select_t peripheral);
    void (*enable_all_peripherals) (void);
    void (*disable_peripheral) (pwr_peripheral_select_t peripheral);
    void (*disable_all_peripherals) (void);
    void (*set_power_saving_mode) (pwr_power_saving_mode_t mode);               // Must unlock before using this function
    boolean_state_t (*is_peripheral_enabled) (pwr_peripheral_select_t peripheral);
}pwr_interface_t;
extern pwr_interface_t pwr;

#endif	/* PWR_H */
