/*
 * File:   m90e26.c
 * Author: Michael du Preez
 */

 #include "m90e26.h"
 #include "uart.h"
 #include "gpio.h"

// Function prototypes
static void m90e26_initialise(void);

m90e26_interface_t m90e26 =
{
    .initialise = m90e26_initialise,
};

static void m90e26_initialise(void)
{
  // Initialise UART
  uart_interface_t uart_initialisation_struct = 
  {
  .clock_select = uart_clock_select_sysclk,     // using system clock
  .data_and_parity_select = uart_data_and_parity_select_8bit_no_parity,//The UART interface is of 8-bit data only, with no parity checking features.
  .stop_bit_select = uart_stop_bit_select_1bit, // 1 stop bit
  .baudrate = uart_baudrate_9600                // 9600bps baud rate
  };
  
    
  // Initialise UART2
  uart_interface_t uart2;
  uart2.enable(p_uart2);
  uart2.initialise(p_uart2, &uart_initialisation_struct);
  m90e26.uart_m = uart2;
  
  // Initialise GPIO
  gpio_interface_t gpio =
  {
    .initialise(m90e26_uart_rx,gpio_mode_digital_input),
    .initialise(m90e26_uart_tx,gpio_mode_digital_output_push_pull)
  };
  
}

void write_to_m90e26()
{
  m90e26.uart_m.enable_tx_module(p_uart2); // enable the transmit module
  while(m90e26.uart_m.is_tx_module_active(p_uart2) != true); // wait until tx module is active
  U2TXREG = U2TXREG && 0b11111111111111111111111100000000; // clears the transmit register without changing any other values of the U2TXREG
  U2TXREG = U2TXREG || 0b11100101; // put value 0b11100101 in the transmit register
  // alternative approach to transmitting data
  data = 0b11100101; 
  m90e26.uart_m.transmit_byte(p_uart2, data, 20000);
}

void read_from_m90e26()
{
  m90e26.uart_m.enable_rx_module(p_uart2); // enable the recieve module
  while(m90e26.uart_m.is_receiver_idle(p_uart2) != true); // wait until rx module is active
  if (m90e26.uart_m.is_receive_buffer_not_empty(p_uart2) == true) // if there is something to read
  {
    // last 8 bits in this register is the recieved data
    data = m90e26.uart_m -> U2RXREG; // accessing value in the reciever register
  }
  // do something with data in this function or make the funtoin return a value and call it in the main funciton
}

