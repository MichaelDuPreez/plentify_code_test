/*
 * File:   base.h
 * Author: Plentify Hardware Team
 */

#ifndef BASE_H
#define	BASE_H

#include <stdint.h>

// Macros
#define mCEILING(value, maximum)                                                        ((value) < (maximum))?(value):(maximum)
#define mFLOOR(value, minimum)                                                          ((value) > (minimum))?(value):(minimum)
#define mBOUND(value, minimum, maximum)                                                 mCEILING(mFLOOR(value, minimum), maximum)
#define mMAP(value, value_minimum, value_maximum, desired_minimum, desired_maximum)     (((((value) - (value_minimum)) * ((desired_maximum) - (desired_minimum)))/((value_maximum) - (value_minimum))) + (desired_minimum))
#define mEXPAND(macro, arg)                                                             macro(arg)

// Constants
#define BIT_0_MASK                      0x01
#define BIT_1_MASK                      0x02
#define BIT_2_MASK                      0x04
#define BIT_3_MASK                      0x08
#define BYTE_DOWN_SHIFTER               8
#define BIT_UP_SHIFTER                  1

// Enums
typedef enum
{
    true = 1,
    false = 0,
    enable = 1,
    disable = 0,
    high = 1,
    low = 0,
    set = 1,
    reset = 0
}boolean_state_t;

#endif	/* BASE_H */
