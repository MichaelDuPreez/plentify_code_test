/*
 * File:   gpio.h
 * Author: Plentify Hardware Team
 */

#ifndef GPIO_H
#define	GPIO_H

#include "pinmap.h"
#include "base.h"

typedef enum
{
    gpio_mode_analogue_input,
    gpio_mode_digital_input,
    gpio_mode_digital_input_pull_down,
    gpio_mode_digital_input_pull_up,
    gpio_mode_digital_output_push_pull,
    gpio_mode_digital_output_open_drain,
    gpio_mode_digital_output_open_drain_pull_up,
} gpio_mode_t;

typedef const struct
{
    void (*initialise) (pinmap_pin_t* p_pin, gpio_mode_t mode);
    boolean_state_t (*read) (pinmap_pin_t* p_pin);
    void (*write) (pinmap_pin_t* p_pin, boolean_state_t value);
    void (*toggle) (pinmap_pin_t* p_pin);
}gpio_interface_t;

extern gpio_interface_t gpio;

#endif	/* GPIO_H */
